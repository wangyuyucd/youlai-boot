package com.youlai.system.model.vo;

import java.util.List;
import lombok.Data;

/**
 * 文件描述
 * *
 * select column_name columnName, data_type dataType, column_comment columnComment, column_key columnKey, extra from
 * information_schema.columns
 * where table_name = #{tableName} and table_schema = (select database()) order by ordinal_position
 *
 * @author yu
 * @date 2023/7/17
 */
@Data
public class GenerateColumnVO {
    private String name;
    private String dataType;
    private String comment;
    private List<String> enumConfig;
}
