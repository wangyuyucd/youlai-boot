package com.youlai.system.model.vo;

import java.time.LocalDateTime;

import lombok.Data;

/**
 * 文件描述
 * <p>
 * select table_name tableName, engine, table_comment tableComment, create_time createTime from information_schema
 * .tables
 * where table_schema = (select database())
 * <p>
 * order by create_time desc
 *
 * @author yu
 * @date 2023/7/17
 */
@Data
public class GenerateTableVO {
    private String name;
    private String comment;
    private LocalDateTime created;
}
