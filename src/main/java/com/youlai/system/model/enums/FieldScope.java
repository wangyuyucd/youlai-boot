package com.youlai.system.model.enums;

public enum FieldScope {
    List, DETAIL;
}
