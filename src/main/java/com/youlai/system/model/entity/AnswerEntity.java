package com.youlai.system.model.entity;

import com.youlai.system.model.entity.base.BizBaseEntity;

/**
 * 回答实体
 *
 * @author yu
 * @date 2023/7/12
 */
public class AnswerEntity extends BizBaseEntity {

    private Long questionId;

    private String content;

}
