package com.youlai.system.model.entity.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * AbstractEntity
 *
 * @author yu.wang3
 * @date 2022/3/28 14:51
 **/
@Accessors(chain = true)
@ToString
public abstract class AbstractEntity<T extends Serializable> implements Serializable {

    @TableId(type = IdType.AUTO)
    private T id;

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }
}
