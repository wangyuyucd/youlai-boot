package com.youlai.system.model.entity.base;

import java.time.LocalDateTime;
import lombok.Data;

/**
 * BizBaseEntity
 *
 * @author yu.wang3
 * @date 2022/3/28 14:51
 **/
@Data
public abstract class BizBaseEntity extends AbstractEntity<Long> {

    @Override
    public Long getId() {
        return super.getId();
    }

    /**
     * 创建时间
     */
    private LocalDateTime created;
    /**
     * 更新时间
     */
    private LocalDateTime modified;

}
