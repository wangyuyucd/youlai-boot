package com.youlai.system.model.query;

import com.youlai.system.common.base.BaseEntity;
import com.youlai.system.common.base.BasePageQuery;
import com.youlai.system.model.base.BasePageQueryDTO;
import com.youlai.system.model.entity.base.BizBaseEntity;
import com.youlai.system.model.enums.FieldScope;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import java.util.Map;
import lombok.Data;

@Schema
@Data
public class GenerateCodeDTO {

    private String name;
    private String desc;

    static class Field {

        private String               name;
        private List<FieldScope>     scopeList;
        private Map<Integer, String> enumMap;
    }

}
