package com.youlai.system.model.query;

import com.youlai.system.common.base.BaseEntity;
import com.youlai.system.common.base.BasePageQuery;
import com.youlai.system.model.base.BasePageQueryDTO;
import com.youlai.system.model.entity.base.BizBaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
@Schema
@Data
public class GenerateTablePageQuery extends BasePageQueryDTO<BizBaseEntity> {

    @Schema(description="表名/备注")
    private String keywords;

}
