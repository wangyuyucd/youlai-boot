package com.youlai.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youlai.system.mapper.QuestionMapper;
import com.youlai.system.model.entity.QuestionEntity;
import com.youlai.system.service.QuestionService;

/**
 * 文件描述
 *
 * @author yu
 * @date 2023/7/16
 */
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, QuestionEntity> implements QuestionService {

}
