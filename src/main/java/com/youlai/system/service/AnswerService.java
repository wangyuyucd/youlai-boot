package com.youlai.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.youlai.system.model.entity.AnswerEntity;
import com.youlai.system.model.entity.QuestionEntity;

public interface AnswerService extends IService<AnswerEntity> {

}
