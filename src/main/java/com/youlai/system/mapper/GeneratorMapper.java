package com.youlai.system.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.model.query.GenerateTablePageQuery;
import com.youlai.system.model.vo.GenerateColumnVO;
import com.youlai.system.model.vo.GenerateTableVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface GeneratorMapper {

    IPage<GenerateTableVO> queryList(IPage<?> page, @Param("query") GenerateTablePageQuery query);

    List<GenerateColumnVO> queryColumns(@Param("tableName") String tableName);

}
