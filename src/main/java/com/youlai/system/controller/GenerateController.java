package com.youlai.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youlai.system.common.annotation.PreventDuplicateSubmit;
import com.youlai.system.common.model.Option;
import com.youlai.system.common.result.PageResult;
import com.youlai.system.common.result.Result;
import com.youlai.system.mapper.GeneratorMapper;
import com.youlai.system.model.form.MenuForm;
import com.youlai.system.model.query.GenerateTablePageQuery;
import com.youlai.system.model.query.MenuQuery;
import com.youlai.system.model.vo.DictPageVO;
import com.youlai.system.model.vo.GenerateColumnVO;
import com.youlai.system.model.vo.GenerateTableVO;
import com.youlai.system.model.vo.MenuVO;
import com.youlai.system.model.vo.RouteVO;
import com.youlai.system.service.SysMenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Tag(name = "08.代码生成接口")
@RestController
@RequestMapping("/api/v1/generate")
@RequiredArgsConstructor
@Slf4j
public class GenerateController {

    private final GeneratorMapper generatorMapper;

    @Operation(summary = "数据表分页列表", security = {@SecurityRequirement(name = "Authorization")})
    @GetMapping("/page")
    public Result<IPage<GenerateTableVO>> page(GenerateTablePageQuery query) {
        IPage<GenerateTableVO> tableList = generatorMapper.queryList(query, query);
        return Result.success(tableList);
    }

    @GetMapping("/columnList")
    public Result<List<GenerateColumnVO>> page(String tableName) {
        List<GenerateColumnVO> tableList = generatorMapper.queryColumns(tableName);
        return Result.success(tableList);
    }
}

